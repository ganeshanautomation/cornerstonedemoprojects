# Author Ganeshan Lakshmi Narayan
@OrderPlacement
Feature:  Test the Order Placement Functionality

  @onelineitem
  Scenario Outline: Place an Order for single line Item
    Given Customer is on <environment> home page
    When Customer search <productType> product and add to the basket with <quantity> quantity and proceed to checkout
    And Customer enters shipping details as <userType> user and complete the order with <paymentType> payment in checkout
    Then Customer navigated to the order confirmation page

    Examples:
      | environment    | productType | quantity | userType   | paymentType |
      | testURL        | wire basket | 1        | Guest      | VISA        |
      | integrationURL | dustpan     | 10       | Registered | AMEX        |

  @multilineitems
  Scenario Outline: Place an Order for multiple line items
    Given Customer is on <environment> home page
    When Customer search <productType> products and add to the basket with <quantity> quantity respectively and proceed to checkout
    And Customer enters shipping details as <userType> user and complete the order with <paymentType> payment in checkout
    Then Customer navigated to the order confirmation page

    Examples:
      | environment    | productType        | quantity | userType   | paymentType |
      | testURL        | wire basket, brush | 1,1      | Guest      | MASTERCARD  |
      | integrationURL | dustpan, brush     | 3,5      | Registered | MASTERO     |
