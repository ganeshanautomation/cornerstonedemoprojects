package stepDefinitions;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageFactory.*;

public class PlaceOrder {

    WebDriver driver = null;

    @Given("^Customer is on (.*) home page$")
    public void customer_is_on_home_page(String environment) {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        driver = new ChromeDriver();
        new HomePage(driver).launchHomePage(environment);
    }

    @When("^Customer search (.*) product and add to the basket with (\\d+) quantity and proceed to checkout$")
    public void customer_search_product_and_add_to_the_basket_with_quantity_and_proceed_to_checkout(String productType, int quantity) {
        new HomePage(driver).searchProduct(productType);
        new SearchPage(driver).selectTheProduct(productType);
        new ProductDisplayPage(driver).addProductToBasket(quantity);
    }

    @When("^Customer search (.*) products and add to the basket with (.*) quantity respectively and proceed to checkout$")
    public void customer_search_products_and_add_to_the_basket_with_quantity_respectively_and_proceed_to_checkout(String productTypes, String quantity) {
        String[] inputProductTypes = productTypes.split(",");
        String[] inputQuantity = quantity.split(",");
        for (int i = 1; i <= inputProductTypes.length && i <= inputQuantity.length; i++) {
            new HomePage(driver).searchProduct(inputProductTypes[i - 1]);
            new SearchPage(driver).selectTheProduct(inputProductTypes[i - 1]);
            if (i == inputProductTypes.length)
                new ProductDisplayPage(driver).addProductToBasket(Integer.parseInt(inputQuantity[i - 1]));
            else
                new ProductDisplayPage(driver).addMultipleProductsToBasket(Integer.parseInt(inputQuantity[i - 1]));
        }
    }

    @And("^Customer enters shipping details as (.*) user and complete the order with (.*) payment in checkout$")
    public void customer_enters_shipping_details_as_user_and_complete_the_order_with_payment_in_checkout(String userType, String paymentType) {
        new CheckOutPage(driver).EnterCustomerDetails(userType, paymentType);
    }

    @Then("Customer navigated to the order confirmation page")
    public void customer_navigated_to_the_order_confirmation_page() {
        System.out.println("Your Order Number is ---> " + new OrderConfirmationPage(driver).getTheOrderNumber());
    }
}
