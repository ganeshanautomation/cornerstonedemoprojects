package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import utils.JsonDataReader;

import java.util.concurrent.ThreadLocalRandom;

public class CheckOutPage extends PageUtils {

    WebDriver driver;

    @FindBy(id = "email")
    WebElement txt_Email;

    @FindBy(id = "password")
    WebElement txt_password;

    @FindBy(id = "checkout-customer-continue")
    WebElement btn_signIn;

    @FindBy(id = "privacyPolicy")
    WebElement chk_Privacy;

    @FindBy(id = "checkout-customer-continue")
    WebElement btn_ContinueAsGuest;

    @FindBy(id = "countryCodeInput")
    WebElement country;

    @FindBy(id = "firstNameInput")
    WebElement txt_firstName;

    @FindBy(id = "lastNameInput")
    WebElement txt_lastName;

    @FindBy(id = "addressLine1Input")
    WebElement txt_addressLine;

    @FindBy(id = "cityInput")
    WebElement txt_city;

    @FindBy(id = "postCodeInput")
    WebElement txt_postCode;

    @FindBy(id = "phoneInput")
    WebElement txt_phoneNumber;

    @FindBy(xpath = "//*[@id=\"checkout-shipping-continue\"]")
    WebElement btn_ContinueToBilling;

    @FindBy(id = "ccNumber")
    WebElement creditCardNumber;

    @FindBy(id = "ccExpiry")
    WebElement expiryDate;

    @FindBy(id = "ccName")
    WebElement creditCardName;

    @FindBy(id = "ccCvv")
    WebElement ccvNumber;

    @FindBy(id = "checkout-payment-continue")
    WebElement submitOrder;

    String userType, paymentType;

    public CheckOutPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void EnterCustomerDetails(String userType, String paymentType) {
        this.userType = userType;
        this.paymentType = paymentType;
        waitForPageToLoad();
        String guestNumber = String.valueOf(ThreadLocalRandom.current().nextInt());
        txt_Email.sendKeys(JsonDataReader.getJsonData("CustomerDetails", userType, "emailId").replace("{RANDOM_NUM}", guestNumber));
        clickByJS(chk_Privacy);
        clickByJS(btn_ContinueAsGuest);
        waitForLoadingToGone();
        switch (userType) {
            case "Guest":
                proceedAsGuestUser();
                break;
            case "Registered":
                proceedAsRegisteredUser();
                break;
        }
    }

    private void proceedAsGuestUser() {
        new Select(country).selectByVisibleText(JsonDataReader.getJsonData("CustomerDetails", userType, "country"));
        txt_firstName.sendKeys(JsonDataReader.getJsonData("CustomerDetails", userType, "firstName"));
        txt_lastName.sendKeys(JsonDataReader.getJsonData("CustomerDetails", userType, "lastName"));
        txt_addressLine.sendKeys(JsonDataReader.getJsonData("CustomerDetails", userType, "addressLine"));
        txt_city.sendKeys(JsonDataReader.getJsonData("CustomerDetails", userType, "city"));
        txt_postCode.sendKeys(JsonDataReader.getJsonData("CustomerDetails", userType, "postCode"));
        txt_phoneNumber.sendKeys(JsonDataReader.getJsonData("CustomerDetails", userType, "phoneNumber"));
        completeOrder();
    }

    private void proceedAsRegisteredUser() {
        txt_password.sendKeys(JsonDataReader.getJsonData("CustomerDetails", userType, "password"));
        clickByJS(btn_signIn);
        completeOrder();
    }

    private void completeOrder() {
        waitAndClickByJS(btn_ContinueToBilling);
        creditCardNumber.sendKeys(JsonDataReader.getJsonData("PaymentCardDetails", paymentType, "creditCardNumber"));
        expiryDate.sendKeys(JsonDataReader.getJsonData("PaymentCardDetails", paymentType, "expiryDate"));
        creditCardName.sendKeys(JsonDataReader.getJsonData("PaymentCardDetails", paymentType, "creditCardName"));
        ccvNumber.sendKeys(JsonDataReader.getJsonData("PaymentCardDetails", paymentType, "ccvNumber"));
        clickByJS(submitOrder);
    }
}
