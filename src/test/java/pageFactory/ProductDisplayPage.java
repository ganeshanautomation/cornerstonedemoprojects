package pageFactory;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductDisplayPage extends PageUtils {

    WebDriver driver;

    @FindBy(id = "qty[]")
    WebElement txt_Quantity;

    @FindBy(id = "form-action-addToCart")
    WebElement btn_AddToCart;

    @FindBy(xpath = "(//a[contains(text(),'Proceed to checkout')])")
    WebElement btn_ProceedToCheckout;

    @FindBy(xpath = "(//a[contains(text(),'Continue Shopping')])")
    WebElement btn_ProceedToContinue;

    public ProductDisplayPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public void addProductToBasket(int quantity) {
        waitForPageToLoad();
        txt_Quantity.sendKeys(Keys.BACK_SPACE);
        txt_Quantity.sendKeys(Integer.toString(quantity));
        clickByJS(btn_AddToCart);
        proceedToCheckout();
    }

    public void addMultipleProductsToBasket(int quantity) {
        waitForPageToLoad();
        txt_Quantity.sendKeys(Keys.BACK_SPACE);
        txt_Quantity.sendKeys(Integer.toString(quantity));
        clickByJS(btn_AddToCart);
        proceedToContinue();
    }

    private void proceedToCheckout() {
        waitForPopUpToLoad();
        clickByJS(btn_ProceedToCheckout);
    }

    private void proceedToContinue() {
        waitForPopUpToLoad();
        clickByJS(btn_ProceedToContinue);
    }
}
