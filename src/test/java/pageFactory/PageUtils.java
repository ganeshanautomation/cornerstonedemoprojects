package pageFactory;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.time.Duration.ofSeconds;

public class PageUtils {

    WebDriver driver;

    public PageUtils(WebDriver driver) {
        this.driver = driver;
    }

    protected void waitForPageToLoad() {
        driver.manage().timeouts().pageLoadTimeout(ofSeconds(40));
    }

    protected void waitForPopUpToLoad() {
        driver.manage().timeouts().implicitlyWait(ofSeconds(30));
    }

    protected void waitForLoadingToGone() {
        driver.manage().timeouts().implicitlyWait(ofSeconds(30));
    }

    protected void clickByJS(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
    }

    protected void waitAndClickByJS(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);

    }
}
