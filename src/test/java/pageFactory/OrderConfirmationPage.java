package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OrderConfirmationPage extends PageUtils {

    @FindBy(xpath = "(//strong)")
    WebElement orderNumber;

    public OrderConfirmationPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getTheOrderNumber() {
        waitForPageToLoad();
        String orderNumberFromUI;
        orderNumberFromUI = orderNumber.getText();
        driver.close();
        driver.quit();
        return orderNumberFromUI;
    }

}
