package pageFactory;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.JsonDataReader;


public class HomePage extends PageUtils {

    WebDriver driver;

    @FindBy(id = "quick-search-expand")
    WebElement btn_Search;

    @FindBy(id = "nav-quick-search")
    WebElement txt_SearchBox;

    public HomePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void launchHomePage(String environment) {
        String url = JsonDataReader.getJsonData("URL", "website", environment);
        driver.get(url);
        waitForPageToLoad();
        driver.manage().window().maximize();
    }

    public void searchProduct(String productType) {
        clickByJS(btn_Search);
        txt_SearchBox.sendKeys(productType);
        txt_SearchBox.sendKeys(Keys.ENTER);
    }
}
