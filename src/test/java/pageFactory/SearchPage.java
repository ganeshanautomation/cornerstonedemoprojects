package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchPage extends PageUtils {

    WebDriver driver;

    @FindBy(xpath = "(//h3[@class='card-title']/a)")
    List<WebElement> searchResults;

    public SearchPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void selectTheProduct(String productType) {
        waitForPageToLoad();
        for (WebElement searchResult : searchResults) {
            if(searchResult.getText().toLowerCase().contains(productType)) {
                clickByJS(searchResult);
                waitForPageToLoad();
                break;
            }
        }
    }
}
